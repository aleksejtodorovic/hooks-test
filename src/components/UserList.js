import React from 'react';

import useResources from './useResources';

const UserList = () => {
    const userList = useResources('users');

    return (
        <div>
            <ul>
                {userList.map(user => {
                    return (
                        <li key={user.id}>{user.name}</li>
                    );
                })}
            </ul>
        </div>
    );
}

export default UserList;