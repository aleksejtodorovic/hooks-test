import { useState, useEffect } from 'react';
import axios from 'axios';

const useResources = (resourceType) => {
    const [resouceList, setResourceList] = useState([]);

    const fetchResourceList = async () => {
        const resourceList = await axios.get(`https://jsonplaceholder.typicode.com/${resourceType}`);

        setResourceList(resourceList.data);
    };

    useEffect(() => {
        fetchResourceList();
    }, [resourceType]);

    return resouceList;
};

export default useResources;