import React from 'react';

import useResources from './useResources';

const ResourceList = ({ resourceType }) => {
    const resourceList = useResources(resourceType);

    return (
        <div>
            <ul>
                {resourceList.map(resource => {
                    return (
                        <li key={resource.id}>{resource.title}</li>
                    );
                })}
            </ul>
        </div>
    );
}

export default ResourceList;